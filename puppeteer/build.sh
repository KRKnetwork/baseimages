#!/bin/bash
ScriptPath="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/"
cd $ScriptPath

docker pull node:lts

docker build \
  -t registry.gitlab.com/krknetwork/baseimages/puppeteer:lts \
  .

docker push registry.gitlab.com/krknetwork/baseimages/puppeteer:lts
